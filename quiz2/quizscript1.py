picture = pickAFile()
def aFunction(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)
  
  for x in range(0,xmax):
    for y in range(0,ymax):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      if b <= 149:
        setBlue(pixel, 255)
        setRed(pixel, 255)
        setGreen(pixel, 255)
      
  repaint(picture)






myPic = makePicture(picture)
changedPic = aFunction(myPic)
show(changedPic)