setMediaPath('/Users/jhoeflich2017/Documents/week4/frames')
def createFrames():
  for r in range(0,256):
    frames = makeEmptyPicture(256,256)
  
    for g in range(0,256):
      for b in range(0,256):
        pixel = getPixel(frames,g,b)
    
        color = makeColor(r, g, b)
        setColor(pixel, color)
      
    writePictureTo(frames, 'frame' + str(r) + '.png')

#movie = makeMovieFromInitialFile('frame0')
createFrames()